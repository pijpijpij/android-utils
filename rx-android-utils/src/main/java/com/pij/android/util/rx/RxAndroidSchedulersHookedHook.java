package com.pij.android.util.rx;


import com.pij.util.rx.Hook;

import rx.android.plugins.RxAndroidSchedulersHook;
import rx.functions.Action0;

import static com.pij.util.rx.RxSchedulersHookedHook.PASS_THROUGH;

/**
 * <p>Set this up at the start of any RxJava application so the hook can dynamically be changed later during the
 * lifetime</p> of the application. <p>Created on 13/06/2016.</p>
 * @author Pierrejean
 */
public final class RxAndroidSchedulersHookedHook extends RxAndroidSchedulersHook {

    private static RxAndroidSchedulersHookedHook instance;
    private Hook delegate;

    public static synchronized RxAndroidSchedulersHookedHook getInstance() {
        if (instance == null) instance = new RxAndroidSchedulersHookedHook();
        return instance;
    }

    private RxAndroidSchedulersHookedHook() {
        delegate = PASS_THROUGH;
        // Apparently redundant, but ensures this class is independent of the implementation of PASS_THROUGH.
        delegate.onStart();
    }

    @Override
    public Action0 onSchedule(Action0 action) {
        return delegate.onSchedule(action);
    }

    public void setDelegate(final Hook newValue) {
        delegate.onStop();
        delegate = newValue == null ? PASS_THROUGH : newValue;
        delegate.onStart();
    }

}
