package com.pij.hamcrest;


import org.hamcrest.Matcher;

/**
 * <p>Created on 02/05/2017.</p>
 * @author Pierrejean
 */
public class Matchers {

    /** Allows applying a matcher to a transformed result. This can be used for example to match against a property
     * of a class (that's useful in Android where Hamcrest's {@link org.hamcrest.Matchers#hasProperty}.
     */
    public static <I, O> Matcher<I> transformed(Transformer<I, O> transform, Matcher<O> matcher) {
        return new Transformed<>(matcher, transform);
    }

}