package com.pij.hamcrest;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * <p>Created on 02/05/2017.</p>
 * @author Pierrejean
 */
@SuppressWarnings("WeakerAccess")
public class Transformed<I, O> extends TypeSafeMatcher<I> {

    private final Matcher<O> delegate;
    private final Transformer<I, O> transformer;

    public Transformed(Matcher<O> delegate, Transformer<I, O> transformer) {
        this.delegate = delegate;
        this.transformer = transformer;
    }

    @Override
    protected boolean matchesSafely(I item) {
        return delegate.matches(transformer.transform(item));
    }

    public void describeTo(Description description) {
        description.appendText("a transformed ").appendDescriptionOf(delegate);
    }

}
