package com.pij.hamcrest;

/**
 * <p>Created on 02/05/2017.</p>
 * @author Pierrejean
 */
@SuppressWarnings("WeakerAccess")
public interface Transformer<I, O> {

    O transform(I input);
}
