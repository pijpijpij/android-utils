Change Log
==========

Version 1.2.4
-------------

Feature:
  * Fix: Android implementation of logger does not log exception stack.

Version 1.2.3
-------------

Feature:
  * Very basic Logger facility and 2 implementation: a Sysout in `utils` libary and Android one in `android-utils`.

Version 1.2.2
-------------

Tec:
  * minor build or doc improvements.

Version 1.2.1
-------------

Feature:
  * Project is buildable by Bitbucket pipelines.

Version 1.2.0
-------------

Feature:
  * Added a Hamcrest library.
  * Upgraded Gradle and Android


Version 1.1.1
-------------

Fixes:
  * Typo and class name of decorated object in CountingHook.
  * a few 3rd party library updates.


Version 1.1.0
-------------

Introduced RxJava utilities modules:
  * `rx-utils` for run an Observable in the background and its subscriber in the foreground.
  * `rx-test` contain a RxJava scheduler hook (`RxSchedulersHookHost`)and an Espresso rule that manages it
    to allow Espresso to wait for RxJava actions.

Version 1.0.2
-------------

  * Re-house the Gate Utility.

Version 1.0.1
-------------

  * Fix: Targeting Java 7 (rather than default).

Version 1.0.0
-------------

  * Introduced Gate Utility.

Version 0.9.0
-------------

  * Internal re-organisation.

Version 0.2.1
-------------

  * Only build management changes.

Version 0.2.0
-------------

  * Added an [UncaughtExceptionHandler](https://docs.oracle.com/javase/7/docs/api/java/lang/Thread.UncaughtExceptionHandler.html)  decorator that logs the exception on Logcat before calling the handler it decorates.

Older Versions
--------------

Not recorded.