package com.pij.rxtest;


import android.support.annotation.NonNull;
import android.support.test.espresso.idling.CountingIdlingResource;

import com.pij.util.rx.Hook;

import rx.functions.Action0;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * <p>Created on 13/06/2016.</p>
 * @author Pierrejean
 */
public final class RxIdlingMonitor implements Hook {

    private final CountingIdlingResource counter;

    public RxIdlingMonitor(@NonNull CountingIdlingResource counter) {
        this.counter = notNull(counter);
    }

    @Override
    public void onStart() {
        // Does nothing
    }

    @NonNull
    @Override
    public Action0 onSchedule(@NonNull final Action0 action) {
        return new Action0() {
            @Override
            public void call() {
                counter.increment();
                try {
                    action.call();
                } finally {
                    counter.decrement();
                }
            }
        };
    }

    @Override
    public void onStop() {
        // Does nothing
    }

}
