package com.pij.rxtest;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.idling.CountingIdlingResource;

import com.pij.util.rx.CountingHook;
import com.pij.util.rx.Hook;
import com.pij.util.rx.RxSchedulersHookedHook;

import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * <p>Created on 13/06/2016.</p>
 * @author Pierrejean
 */
public class RxIdlingRule implements TestRule {

    public static final Hook COUNTING_PASS_THROUGH = new CountingHook(RxSchedulersHookedHook.PASS_THROUGH);

    private final boolean debug;

    public RxIdlingRule(boolean debug) {
        this.debug = debug;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        String resourceName = getClass().getSimpleName() + " " + description.getMethodName();
        CounterManager wrapper = new CounterManager(resourceName, debug);
        return wrapper.apply(base, description);
    }

    private static class CounterManager extends TestWatcher {

        private final CountingIdlingResource counter;
        private final boolean debug;

        private CounterManager(String resourceName, boolean debug) {
            this.counter = new CountingIdlingResource(resourceName, debug);
            this.debug = debug;
        }

        @Override
        protected void starting(Description description) {
            Hook hook = new RxIdlingMonitor(counter);
            if (debug) hook = new CountingHook(hook);
            RxSchedulersHookedHook.getInstance().setDelegate(hook);
            Espresso.registerIdlingResources(counter);
        }

        @Override
        protected void finished(Description description) {
            Espresso.unregisterIdlingResources(counter);
            Hook newHook = debug ? COUNTING_PASS_THROUGH : null;
            RxSchedulersHookedHook.getInstance().setDelegate(newHook);
        }

    }
}
