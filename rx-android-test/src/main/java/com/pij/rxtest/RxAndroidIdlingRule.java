package com.pij.rxtest;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.idling.CountingIdlingResource;

import com.pij.android.util.rx.RxAndroidSchedulersHookedHook;

import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * <p>Created on 13/06/2016.</p>
 * @author Pierrejean
 */
public class RxAndroidIdlingRule implements TestRule {

    private final boolean debug;

    public RxAndroidIdlingRule() {
        this(false);
    }

    public RxAndroidIdlingRule(boolean debug) {
        this.debug = debug;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        String resourceName = getClass().getSimpleName() + " " + description.getMethodName();
        CounterManager wrapper = new CounterManager(resourceName, debug);
        return wrapper.apply(base, description);
    }

    private static class CounterManager extends TestWatcher {

        private final CountingIdlingResource counter;

        private CounterManager(String resourceName, boolean debug) {
            counter = new CountingIdlingResource(resourceName, debug);
        }

        @Override
        protected void starting(Description description) {
            RxAndroidSchedulersHookedHook.getInstance().setDelegate(new RxIdlingMonitor(counter));
            Espresso.registerIdlingResources(counter);
        }

        @Override
        protected void finished(Description description) {
            if (!counter.isIdleNow()) {
                counter.dumpStateToLogs();
            }
            Espresso.unregisterIdlingResources(counter);
            RxAndroidSchedulersHookedHook.getInstance().setDelegate(null);
        }

    }
}
