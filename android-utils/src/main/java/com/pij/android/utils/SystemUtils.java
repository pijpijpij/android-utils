package com.pij.android.utils;

import android.content.ContentUris;
import android.net.Uri;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * @author pierrejean
 */
public abstract class SystemUtils extends com.pij.utils.SystemUtils {

    /**
     * Converts a system time into an elapsed time (obtained through {@link SystemClock#elapsedRealtime()}.
     * @param systemTime obtained through {@link System#currentTimeMillis()}
     */
    public static long convertSystemToElapsedTime(long systemTime) {

        final long phoneBootTime = System.currentTimeMillis() - SystemClock.elapsedRealtime();
        //noinspection UnnecessaryLocalVariable
        final long elapsedRealTime = systemTime - phoneBootTime;
        return elapsedRealTime;
    }

    /**
     * Similar to {@link ContentUris#parseId(Uri)}, but logs the error before exploding!
     * @return the id of the item in the URI. If the URI is not a valid entity, it returns -1 or throws.
     * @see ContentUris#parseId(Uri)
     */
    public static long parseId(@NonNull Uri uri) {

        try {
            //noinspection UnnecessaryLocalVariable
            final long result = ContentUris.parseId(uri);
            return result;
        } catch (UnsupportedOperationException | NumberFormatException e) {
            Log.d(tag(SystemUtils.class), "Could not extract an id from " + uri, e);
            throw e;
        }
    }

    /**
     * To allow extension.
     */
    protected SystemUtils() {

        // Does nothing.
    }

}
