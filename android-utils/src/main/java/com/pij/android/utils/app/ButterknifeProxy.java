package com.pij.android.utils.app;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;

/**
 * @author Pierrejean on 25/09/2015.
 */
interface ButterknifeProxy {

    void bind(@NonNull Activity target);

    void unbind(@NonNull Activity target);

    void bind(@NonNull Fragment target, @NonNull View source);

    void unbind(@NonNull Fragment target);
}
