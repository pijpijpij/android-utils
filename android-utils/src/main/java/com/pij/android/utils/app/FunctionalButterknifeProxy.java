package com.pij.android.utils.app;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import butterknife.ButterKnife;

import static com.pij.android.utils.SystemUtils.tag;

/**
 * @author Pierrejean on 26/09/2015.
 */
class FunctionalButterknifeProxy implements ButterknifeProxy {

    @Override
    public void bind(@NonNull Activity target) {
        Log.i(tag(target), "Butterknife enabled.");
        ButterKnife.bind(target);
    }

    @Override
    public void unbind(@NonNull Activity target) {
        ButterKnife.unbind(target);
    }

    @Override
    public void bind(@NonNull Fragment target, @NonNull View source) {
        Log.i(tag(target), "Butterknife enabled.");
        ButterKnife.bind(target, source);
    }

    @Override
    public void unbind(@NonNull Fragment target) {
        ButterKnife.unbind(target);
    }

}
