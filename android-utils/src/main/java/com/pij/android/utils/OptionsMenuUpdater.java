package com.pij.android.utils;

import android.app.Activity;
import android.database.DataSetObserver;
import android.support.annotation.NonNull;

import static org.apache.commons.lang3.Validate.notNull;

public final class OptionsMenuUpdater extends DataSetObserver {

    private final Activity activity;

    public OptionsMenuUpdater(@NonNull Activity activity) {
        this.activity = notNull(activity);
    }

    @Override
    public void onChanged() {
        update();
    }

    @Override
    public void onInvalidated() {
        update();
    }

    public void update() {
        activity.invalidateOptionsMenu();
    }
}
