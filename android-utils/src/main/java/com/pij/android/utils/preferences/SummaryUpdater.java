package com.pij.android.util.preferences;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.support.annotation.NonNull;

/**
 * Provides implementation to update a summary of a preference. Typical use, with IntegerSummaryUpdater as an example:
 * <p/>
 * <pre>
 * private Preference preference;
 * private SummaryUpdater<Integer> preferenceUpdater = new IntegerSummaryUpdater(R.plural.my_summary);
 *
 * <pre>
 * Then in onCreate():
 *
 * <pre>
 * preferenceUpdater.updateSummary(top);
 * </pre>
 * <p/>
 * Later, in onResume():
 * <p/>
 * <pre>
 * preference.setOnPreferenceChangeListener(preferenceUpdater);
 * </pre>
 * <p/>
 * You can guess what is placed in onPause().
 * @author pierrejean
 */
abstract class SummaryUpdater<T> implements OnPreferenceChangeListener {

    @Override
    public final boolean onPreferenceChange(Preference preference, Object newValue) {
        updateSummary(preference, newValue);
        return true;
    }

    public final void updateSummary(@NonNull Preference preference) {
        SharedPreferences prefs = preference.getSharedPreferences();
        String key = preference.getKey();
        updateSummary(preference, extractValue(prefs, key));
    }

    private void updateSummary(@NonNull Preference preference, Object newValue) {
        Resources resources = preference.getContext().getResources();
        preference.setSummary(calculateSummary(resources, newValue));
    }

    protected abstract String calculateSummary(@NonNull Resources resources, Object preferenceValue);

    protected abstract T extractValue(@NonNull SharedPreferences prefs, @NonNull String key);
}
