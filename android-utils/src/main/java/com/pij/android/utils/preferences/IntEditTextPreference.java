package com.pij.android.util.preferences;

import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.util.Log;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * Minimalist implementation of an integer preference editor.
 * @author pierrejean
 */
public class IntEditTextPreference extends EditTextPreference {

    public IntEditTextPreference(Context context) {
        super(context);
    }

    public IntEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IntEditTextPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected boolean persistString(String value) {
        final int intValue = NumberUtils.toInt(value);
        try {
            return persistInt(intValue);
        } catch (ClassCastException absorbed) {
            logNotAnInteger();
            return getEditor().clear().putInt(getKey(), intValue).commit();
        }
    }

    @Override
    protected String getPersistedString(String defaultReturnValue) {
        try {
            return String.valueOf(getPersistedInt(-1));
        } catch (ClassCastException absorbed) {
            logNotAnInteger();
            return defaultReturnValue;
        }
    }

    private void logNotAnInteger() {
        Log.w(getClass().getSimpleName(), "Key " + getKey() + " is not an integer");
    }
}
