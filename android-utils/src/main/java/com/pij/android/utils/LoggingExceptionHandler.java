package com.pij.android.utils;

import android.support.annotation.NonNull;
import android.util.Log;

import java.lang.Thread.UncaughtExceptionHandler;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

/**
 * Added an {@link UncaughtExceptionHandler} decorator that logs the exception on Logcat before calling the handler it
 * decorates.
 * @author Pierrejean
 */
public class LoggingExceptionHandler implements UncaughtExceptionHandler {

    @NonNull
    private final UncaughtExceptionHandler chain;

    /**
     * @param chain if <code>null</code>, this instance will simply log exceptions
     */
    public LoggingExceptionHandler(UncaughtExceptionHandler chain) {
        this.chain = defaultIfNull(chain, new Noop());
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        log(ex);
        chain.uncaughtException(thread, ex);
    }

    private void log(Throwable ex) {
        Log.wtf(getClass().getSimpleName(), "Uncaught exception:", ex);
    }

    private static class Noop implements UncaughtExceptionHandler {

        @Override
        public void uncaughtException(Thread thread, Throwable ex) {

        }
    }
}
