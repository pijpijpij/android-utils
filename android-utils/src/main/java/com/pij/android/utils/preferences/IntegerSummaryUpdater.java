package com.pij.android.util.preferences;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.annotation.PluralsRes;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Uses a plural to update a preference summary.
 * @author pierrejean
 */
public final class IntegerSummaryUpdater extends SummaryUpdater<Integer> {

    private final int summaryId;

    public IntegerSummaryUpdater(@PluralsRes int summaryId) {
        this.summaryId = summaryId;
    }

    @Override
    protected String calculateSummary(Resources resources, Object preferenceValue) {
        @SuppressWarnings("deprecation")
        int intValue = NumberUtils.toInt(ObjectUtils.toString(preferenceValue, "0"));
        return resources.getQuantityString(summaryId, intValue, intValue);
    }

    @Override
    protected Integer extractValue(SharedPreferences prefs, String key) {
        try {
            return prefs.getInt(key, 0);
        } catch (ClassCastException e) {
            try {
                return Integer.parseInt(prefs.getString(key, "0"));
            } catch (ClassCastException | NumberFormatException absorbed) {
                return 0;
            }
        }
    }
}
