package com.pij.android.util.preferences;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * Deals with Existing preferences of incorrect type.
 * @author pierrejean
 */
public final class PreferenceAccessor {

    private final SharedPreferences preferences;

    public PreferenceAccessor(@NonNull SharedPreferences preferences) {
        this.preferences = notNull(preferences);
    }

    public List<Integer> extractPreferenceSplitAsIntegers(String key, String defaultRawValue) {
        List<String> lines = extractPreferenceSplitAsStrings(key, defaultRawValue);
        List<Integer> result = new ArrayList<>();
        for (String rawLine : lines) {
            String line = StringUtils.trimToNull(rawLine);
            try {
                int column = Integer.parseInt(line);
                result.add(column);
            } catch (final NumberFormatException swallowed) {
                Log.w(getClass().getSimpleName(), "line " + line + " is not an integer");
            }
        }
        return result;
    }

    public List<String> extractPreferenceSplitAsStrings(String key, String defaultRawValue) {
        String rawValue = extractPreferenceSafely(key, defaultRawValue);
        String[] lines = rawValue.split("\\r?\\n");
        List<String> result = new ArrayList<>();
        for (String rawLine : lines) {
            String line = StringUtils.trimToNull(rawLine);
            if (line != null) {
                result.add(line);
            }
        }
        return result;
    }

    public int extractPreferenceSafely(String key, int defaultValue) {
        try {
            return preferences.getInt(key, defaultValue);
        } catch (ClassCastException absorbed) {
            Log.w(getClass().getSimpleName(), "Key " + key + " is not an integer");
            return defaultValue;
        }
    }

    public String extractPreferenceSafely(String key, String defaultValue) {
        try {
            return preferences.getString(key, defaultValue);
        } catch (ClassCastException absorbed) {
            Log.w(getClass().getSimpleName(), "Key " + key + " is not a string");
            return defaultValue;
        }
    }

}
