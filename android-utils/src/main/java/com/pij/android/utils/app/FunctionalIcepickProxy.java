package com.pij.android.utils.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;

import icepick.Icepick;

import static com.pij.android.utils.SystemUtils.tag;

/**
 * @author Pierrejean on 26/09/2015.
 */
class FunctionalIcepickProxy implements IcepickProxy {

    @Override
    public void saveInstanceState(@NonNull Activity source, Bundle savedInstanceState) {
        Log.i(tag(source), "Icepick enabled.");
        Icepick.saveInstanceState(source, savedInstanceState);
    }

    @Override
    public void restoreInstanceState(@NonNull Activity target, Bundle outState) {
        Icepick.restoreInstanceState(target, outState);
    }

    @Override
    public void saveInstanceState(@NonNull Fragment source, Bundle savedInstanceState) {
        Log.i(tag(source), "Icepick enabled.");
        Icepick.saveInstanceState(source, savedInstanceState);
    }

    @Override
    public void restoreInstanceState(@NonNull Fragment target, Bundle outState) {
        Icepick.restoreInstanceState(target, outState);
    }
}
