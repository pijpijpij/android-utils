package com.pij.android.utils;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.pij.noopetal.Noop;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * @author Pierrejean on 13/06/2015.
 */
public final class BackgroundQueryHandler extends AsyncQueryHandler {

    private final Callback sequence;

    public BackgroundQueryHandler(@NonNull ContentResolver contentResolver, @NonNull Callback sequence) {
        super(contentResolver);
        this.sequence = notNull(sequence);
    }

    @Override
    protected void onQueryComplete(final int token, final Object cookie, final Cursor cursor) {
        post(new Runnable() {
            @Override
            public void run() {
                sequence.onQueryComplete(token, cursor);
            }
        });
    }

    @Override
    protected void onInsertComplete(final int token, Object cookie, final Uri uri) {
        post(new Runnable() {
            @Override
            public void run() {
                sequence.onInsertComplete(token, uri);
            }
        });
    }

    @Override
    protected void onUpdateComplete(final int token, final Object cookie, final int result) {
        post(new Runnable() {
            @Override
            public void run() {
                sequence.onUpdateComplete(token, result);
            }
        });
    }

    @Override
    protected void onDeleteComplete(final int token, Object cookie, final int result) {
        post(new Runnable() {
            @Override
            public void run() {
                sequence.onDeleteComplete(token, result);
            }
        });
    }

    @Noop
    public interface Callback {

        void onQueryComplete(int token, Cursor cursor);

        void onDeleteComplete(int token, int result);

        void onInsertComplete(int token, Uri uri);

        void onUpdateComplete(int token, int result);
    }

}
