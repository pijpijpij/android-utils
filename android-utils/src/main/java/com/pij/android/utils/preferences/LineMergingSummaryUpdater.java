package com.pij.android.util.preferences;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.PluralsRes;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Concatenate the lines found in the preference an applies them to a plural string.
 * @author pierrejean
 */
public final class LineMergingSummaryUpdater extends SummaryUpdater<String> {

    private final int summaryId;
    private final String defaultValue;

    public LineMergingSummaryUpdater(@PluralsRes int summaryId, String defaultValue) {
        this.summaryId = summaryId;
        this.defaultValue = defaultValue;
    }

    @Override
    protected String calculateSummary(Resources resources, Object preferenceValue) {
        @SuppressWarnings("deprecation")
        String rawValue = ObjectUtils.toString(preferenceValue, "");
        List<String> lines = extractLines(rawValue);
        String concatenatedLine = concatenate(lines);
        return resources.getQuantityString(summaryId, lines.size(), lines.size(), concatenatedLine);
    }

    @Override
    protected String extractValue(SharedPreferences preferences, String key) {
        try {
            return preferences.getString(key, defaultValue);
        } catch (ClassCastException e) {
            return null;
        }
    }

    @NonNull
    private List<String> extractLines(@NonNull String rawValue) {
        String[] lines = rawValue.split("\\r?\\n");
        List<String> result = new ArrayList<>();
        for (String rawLine : lines) {
            String line = StringUtils.trimToNull(rawLine);
            if (line != null) {
                result.add(line);
            }
        }
        return result;
    }

    @NonNull
    private String concatenate(@NonNull List<String> lines) {
        return StringUtils.join(lines, ", ");
    }
}
