package com.pij.android.utils.widget;

import android.database.DataSetObserver;
import android.support.annotation.NonNull;
import android.support.annotation.PluralsRes;
import android.widget.Adapter;
import android.widget.TextView;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * Displays the count of records off an adapter onto a target view, using a plural.
 */
public class AdapterCountDisplay {

    private final DataSetObserver observer;
    private final Adapter adapter;
    private final int displayResource;
    private TextView target;

    public AdapterCountDisplay(@NonNull Adapter adapter, @PluralsRes int displayResource) {
        this.adapter = notNull(adapter);
        observer = new MyDataSetObserver();
        this.displayResource = displayResource;
    }

    /**
     * Connects the target to the adapter count.
     * @param target if <code>null</code> the updater is disconnected from the adapter
     */
    public void setTarget(TextView target) {
        this.target = target;
        if (adapter != null) {
            if (target == null) {
                adapter.unregisterDataSetObserver(observer);
            } else {
                observer.onChanged();
                adapter.registerDataSetObserver(observer);
            }
        }
    }

    /**
     * Internal class to avoid exposing DataSetObserver mechanism. on the main class' interface.
     */
    private class MyDataSetObserver extends DataSetObserver {

        @Override
        public void onChanged() {
            int count = adapter.getCount();
            CharSequence formattedCount = target.getResources().getQuantityString(displayResource, count, count);
            target.setText(formattedCount);
        }

    }
}
