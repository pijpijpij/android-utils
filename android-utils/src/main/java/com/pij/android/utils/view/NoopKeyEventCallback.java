package com.pij.android.utils.view;

import android.view.KeyEvent;
import android.view.KeyEvent.Callback;

/**
 * Convenience class to code only for the events managed.
 */
public class NoopKeyEventCallback implements Callback {

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
        return false;
    }

}