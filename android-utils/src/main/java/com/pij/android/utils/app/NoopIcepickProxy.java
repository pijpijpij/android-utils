package com.pij.android.utils.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;

import static com.pij.android.utils.SystemUtils.tag;

/**
 * @author Pierrejean on 26/09/2015.
 */
class NoopIcepickProxy implements IcepickProxy {

    /**
     * Does nothing.
     */
    @Override
    public void saveInstanceState(@NonNull Activity ignored, Bundle savedInstanceState) {
        Log.i(tag(ignored), "Icepick disabled.");
    }

    /**
     * Does nothing.
     */
    @Override
    public void restoreInstanceState(@NonNull Activity ignored, Bundle outState) {
        // Does nothing
    }

    /**
     * Does nothing.
     */
    @Override
    public void saveInstanceState(@NonNull Fragment ignored, Bundle savedInstanceState) {
        Log.i(tag(ignored), "Icepick disabled.");
    }

    /**
     * Does nothing.
     */
    @Override
    public void restoreInstanceState(@NonNull Fragment ignored, Bundle outState) {
        // Does nothing
    }

}
