package com.pij.android.utils;

import android.support.annotation.NonNull;
import android.util.Log;

import com.pij.utils.Logger;

/**
 * <p>Created on 22/03/2018.</p>
 *
 * @author Pierrejean
 */

public class AndroidDebugLogger implements Logger {

    private void print(@NonNull Class<?> clazz, @NonNull String message) {
        Log.d(clazz.getSimpleName(), message);
    }

    @Override
    public void print(@NonNull Class<?> clazz, @NonNull String messageTemplate, @NonNull Object... args) {
        print(clazz, String.format(messageTemplate, args));
    }

    @Override
    public void print(@NonNull Class<?> clazz, @NonNull Throwable error, @NonNull String messageTemplate, @NonNull Object... args) {
        Log.d(clazz.getSimpleName(), String.format(messageTemplate, args), error);
    }
}
