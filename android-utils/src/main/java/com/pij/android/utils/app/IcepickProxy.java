package com.pij.android.utils.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

/**
 * @author Pierrejean on 25/09/2015.
 */
interface IcepickProxy {

    void saveInstanceState(@NonNull Activity source, Bundle outState);

    void restoreInstanceState(@NonNull Activity target, Bundle savedInstanceState);

    void saveInstanceState(@NonNull Fragment source, Bundle savedInstanceState);

    void restoreInstanceState(@NonNull Fragment target, Bundle outState);
}
