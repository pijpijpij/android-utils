package com.pij.android.utils.app;

/**
 * @author Pierrejean on 26/09/2015.
 */
public interface Configuration {

    interface Butterknife {

        ButterknifeProxy ACTIVE = new FunctionalButterknifeProxy();
        ButterknifeProxy NOT_ACTIVE = new NoopButterknifeProxy();
    }

    interface Icepick {

        IcepickProxy ACTIVE = new FunctionalIcepickProxy();
        IcepickProxy NOT_ACTIVE = new NoopIcepickProxy();
    }
}
