package com.pij.android.utils;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.pij.android.support.R;

/**
 * @author pierrejean
 */
public abstract class Util {

    /**
     * Logs an unexpected {@link RuntimeException} and puts out a toast.
     * @param caller    caught the exception
     * @param messageId id of the message to localise and print
     */
    public static void log(Activity caller, int messageId) {

        log(caller, caller, messageId);
    }

    /**
     * Logs an error message and puts out a toast.
     * @param caller caught the exception
     */
    public static void log(Activity context, Object caller, int messageId) {

        if (context == null) {

            Log.wtf(SystemUtils.tag(Util.class), "logException()'s context is null");
        } else {
            final String message = context.getString(messageId);
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Logs an unexpected {@link RuntimeException} and puts out a toast.
     * @param caller caught the exception
     * @param e      the exception
     */
    public static void logException(Activity caller, @NonNull Throwable e) {

        logException(caller, caller, e);
    }

    /**
     * Logs an unexpected {@link RuntimeException} and puts out a toast.
     * @param caller caught the exception
     * @param e      the exception
     */
    public static void logException(@NonNull Activity context, Object caller, @NonNull Throwable e) {

        logException(caller, e);
        //noinspection ConstantConditions
        if (context == null) {

            Log.wtf(SystemUtils.tag(Util.class), "logException()'s context is null");
        } else {
            final String message = context.getString(R.string.unexpected_exception);
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Logs an unexpected {@link RuntimeException} and puts out a toast.
     * @param caller caught the exception
     * @param e      the exception
     */
    public static void logException(@NonNull Fragment caller, @NonNull Throwable e) {

        logException(caller, caller, e);
    }

    /**
     * Logs an unexpected {@link RuntimeException} and puts out a toast.
     * @param caller caught the exception
     * @param e      the exception
     */
    public static void logException(@NonNull Fragment context, @NonNull Object caller, @NonNull Throwable e) {

        Activity activity = null;
        //noinspection ConstantConditions
        if (context != null) {
            activity = context.getActivity();
        }
        logException(activity, caller, e);
    }

    /**
     * Logs an unexpected {@link RuntimeException}.
     * @param caller caught the exception
     * @param e      the exception
     */
    public static void logException(@Nullable final Object caller, @NonNull Throwable e) {

        Log.wtf(SystemUtils.tag(caller), "Unexpected exception.", e);
    }

    /**
     * Prints out the column names of a cursor.
     * @param label  the tag to use to log...
     * @param cursor cursor to analyse
     */
    public static void printColumns(@NonNull Cursor cursor, @NonNull String label) {

        String[] columns;
        final StringBuilder buffer = new StringBuilder("Cursor");
        //noinspection ConstantConditions
        if (cursor != null) {
            columns = cursor.getColumnNames();

            buffer.append(" has ");
            buffer.append(columns.length);
            buffer.append(" Columns: ");
            for (String column : columns) {
                buffer.append(column).append(", ");
            }
            buffer.append(" and ").append(cursor.getCount()).append(" rows");
        } else {
            buffer.append(" is null");
        }
        Log.d(label, buffer.toString());
    }

    /**
     * Logs a message describing the exception, as a Toast.
     * @param context used to toast
     * @param e       error that occurred during an operation
     */
    public static void toast(@NonNull Context context, @NonNull CharSequence message, Throwable e) {

        try {
            final Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        } catch (Throwable ignored) {

            Log.w(SystemUtils.tag(Util.class),
                  "Could not put out a Toast (" + (e == null ? null : e.getMessage()) + ")");
        }
    }

    /**
     * Used to debug.
     */
    public static void toastUnsupported(@NonNull Activity context) {
        toast(context, "Unsupported operation", null);

    }

    /**
     * Logs a message describing the exception, both in Logcat and optionally as a Toast.
     * @param uri URI for which the read failed
     * @param e   error that occurred during the read
     */
    public static void toastFailedRead(@NonNull Context context, @NonNull Uri uri, @NonNull Exception e) {
        final String message = context.getString(R.string.error_reading, uri);
        toast(context, message, e);
    }

    /**
     * Logs a message describing the exception as a Toast.
     * @param uri URI for which the insertion failed
     * @param e   error that occurred during the insertion
     */
    public static void toastFailedInsert(@NonNull Context context, @NonNull Uri uri, @NonNull ContentValues values,
                                         @NonNull Exception e) {
        final String message = context.getString(R.string.error_inserting, uri, values);
        toast(context, message, e);
    }

    /**
     * Logs a message describing the exception as a Toast.
     * @param uri URI for which the deletion failed
     * @param e   error that occurred during the deletion
     */
    public static void toastFailedDelete(@NonNull Context context, @NonNull Uri uri, @NonNull SQLException e) {
        final String message = context.getString(R.string.error_deleting, uri);
        toast(context, message, e);
    }

    /**
     * Logs a message describing the exception as a Toast.
     * @param uri URI for which the update failed
     * @param e   error that occurred during the update
     */
    public static void toastFailedUpdate(@NonNull Context context, @NonNull Uri uri, @NonNull ContentValues values,
                                         @NonNull Exception e) {
        final String message = context.getString(R.string.error_updating, uri, values);
        toast(context, message, e);
    }

    /**
     * Utility method. Logs a message describing the exception in Logcat only.
     */
    private static void logFailure(@NonNull String message, @NonNull Exception e) {

        logFailure(Util.class, message, e);
    }

    /**
     * Utility method. Logs a message describing the exception in Logcat only.
     */
    public static void logFailure(@Nullable final Object caller, @NonNull String message, @NonNull Exception e) {

        final Class<?> clazz = caller == null ? Util.class : caller.getClass();
        logFailure(clazz, message, e);
    }

    /**
     * Utility method. Logs a message describing the exception in Logcat only.
     */
    public static void logFailure(@Nullable final Class<?> caller, @NonNull String message, @NonNull Exception e) {

        final Class<?> clazz = caller == null ? Util.class : caller;

        Log.e(SystemUtils.tag(clazz), message, e);
    }

    /**
     * Logs a message describing the exception in Logcat.
     * @param uri URI for which the read failed
     * @param e   error that occurred during the read
     */
    public static void logFailedRead(@NonNull Context context, @NonNull Uri uri, @NonNull Exception e) {

        String message = "No context provided to log";
        //noinspection ConstantConditions
        if (context != null) {
            message = context.getString(R.string.error_reading, uri);
        }
        logFailure(message, e);
    }

    /**
     * Logs a message describing the exception, both in Logcat and optionally as a Toast.
     * @param uri URI for which the insertion failed
     * @param e   error that occurred during the insertion
     */
    public static void logFailedInsert(Context context, @NonNull Uri uri, @NonNull ContentValues values,
                                       @NonNull Exception e) {

        String message = "No context provided to log";
        if (context != null) {
            message = context.getString(R.string.error_inserting, uri, values);
        }
        logFailure(message, e);
    }

    /**
     * Logs a message describing the exception in Logcat.
     * @param uri URI for which the deletion failed
     * @param e   error that occurred during the deletion
     */
    public static void logFailedDelete(Context context, @NonNull Uri uri, @NonNull SQLException e) {
        String message = "No context provided to log";
        if (context != null) {
            message = context.getString(R.string.error_deleting, uri);
        }
        logFailure(message, e);
    }

    /**
     * Convenience method. Logs a message describing the exception in Logcat.
     * @param uri    URI for which the update failed
     * @param values data to insert as a row.
     * @param e      error that occurred during the update
     */
    public static void logFailedUpdate(Context context, @NonNull Uri uri, @NonNull ContentValues values,
                                       @NonNull Exception e) {

        String message = "No context provided to log";
        if (context != null) {
            message = context.getString(R.string.error_updating, uri, values);
        }
        logFailure(message, e);
    }

    /**
     * To avoid instantiation.
     */
    private Util() {

        // Does nothing.
    }

}
