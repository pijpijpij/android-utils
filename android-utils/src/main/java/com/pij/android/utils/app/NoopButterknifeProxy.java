package com.pij.android.utils.app;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import static com.pij.android.utils.SystemUtils.tag;

/**
 * @author Pierrejean on 26/09/2015.
 */
class NoopButterknifeProxy implements ButterknifeProxy {

    /**
     * Does nothing.
     */
    @Override
    public void bind(@NonNull Activity ignored) {
        Log.i(tag(ignored), "Butterknife disabled.");
    }

    /**
     * Does nothing.
     */
    @Override
    public void unbind(@NonNull Activity ignored) {
        // Does nothing
    }

    /**
     * Does nothing.
     */
    @Override
    public void bind(@NonNull Fragment ignored, @NonNull View source) {
        Log.i(tag(ignored), "Butterknife disabled.");
    }

    /**
     * Does nothing.
     */
    @Override
    public void unbind(@NonNull Fragment ignored) {
        // Does nothing
    }

}
