package com.pij.android.utils.widget;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * Exports the business of determining the view/layout to an external object, a {@link ViewProvider}.
 * @author pierrejean
 */
public class MultiLayoutCursorAdapter extends SimpleCursorAdapter {

    private final ViewProvider viewProvider;

    public MultiLayoutCursorAdapter(@NonNull Context context, String[] from, int[] to,
                                    @NonNull ViewProvider calculator) {
        super(context, 0, null, from, to, 0);
        this.viewProvider = notNull(calculator);
    }

    @Override
    public int getItemViewType(int position) {
        if (!getCursor().moveToPosition(position)) {
            throw new SQLException("Failed to mode to position " + position);
        }
        return viewProvider.getItemViewType(getCursor());
    }

    @Override
    public int getViewTypeCount() {
        return viewProvider.getViewTypeCount();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return viewProvider.getView(context, cursor, parent);
    }

    public interface ViewProvider {

        int getViewTypeCount();

        /**
         * @param cursor Assumed to be positioned to the correct position
         * @return a number that identifies the layouot to use for the current row
         */
        int getItemViewType(@NonNull Cursor cursor);

        /**
         * Assume that <code>convertView</code> matches the type of view expected by {@link #getItemViewType(int)}.
         * @param context self-explanatory
         * @param cursor  Assumed to be positioned on the correct position
         * @param parent  parent...
         * @return same as {@link android.widget.Adapter#getView(int, View, ViewGroup)}
         */
        View getView(@NonNull Context context, @NonNull Cursor cursor, ViewGroup parent);

    }

}
