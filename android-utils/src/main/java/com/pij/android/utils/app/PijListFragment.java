package com.pij.android.utils.app;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ListFragment;
import android.view.View;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * @author Pierrejean on 26/09/2015.
 */
public abstract class PijListFragment extends ListFragment {

    private final ButterknifeProxy butterknife;
    private final IcepickProxy icepick;

    /**
     * Butterknife is not active and Icepick is not active.
     */
    public PijListFragment() {this(Configuration.Butterknife.NOT_ACTIVE, Configuration.Icepick.NOT_ACTIVE);}

    /**
     * Butterknife can be activated or not. Icepick can be activated or not.
     * @param butterknife if {@link com.pij.android.utils.app.Configuration.Butterknife#ACTIVE}, then butterknife is
     *                    active. if {@link com.pij.android.utils.app.Configuration.Butterknife#NOT_ACTIVE} it is not
     *                    activated.
     * @param icepick     if {@link com.pij.android.utils.app.Configuration.Icepick#ACTIVE}, then icepick is active. if
     *                    {@link com.pij.android.utils.app.Configuration.Icepick#NOT_ACTIVE} it is not activated.
     */
    public PijListFragment(@NonNull ButterknifeProxy butterknife, @NonNull IcepickProxy icepick) {
        this.butterknife = notNull(butterknife);
        this.icepick = notNull(icepick);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        butterknife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        butterknife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        icepick.saveInstanceState(this, outState);
    }
}
