package com.pij.android.utils.app;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * @author Pierrejean on 25/09/2015.
 */
// TODO Rename to AppCompatActivity
public class PijActivity extends AppCompatActivity implements Configuration {

    private final ButterknifeProxy butterknife;
    private final IcepickProxy icepick;

    /**
     * Butterknife is not active and Icepick is not active.
     */
    public PijActivity() {this(Butterknife.NOT_ACTIVE, Icepick.NOT_ACTIVE);}

    /**
     * Butterknife can be activated or not. Icepick can be activated or not.
     * @param butterknife if {@link com.pij.android.utils.app.Configuration.Butterknife#ACTIVE}, then butterknife is
     *                    active. if {@link com.pij.android.utils.app.Configuration.Butterknife#NOT_ACTIVE} it is not
     *                    activated.
     * @param icepick     if {@link com.pij.android.utils.app.Configuration.Icepick#ACTIVE}, then icepick is active. if
     *                    {@link com.pij.android.utils.app.Configuration.Icepick#NOT_ACTIVE} it is not activated.
     */
    public PijActivity(@NonNull ButterknifeProxy butterknife, @NonNull IcepickProxy icepick) {
        this.butterknife = notNull(butterknife);
        this.icepick = notNull(icepick);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        butterknife.bind(this);
    }

    @Override
    protected void onDestroy() {
        butterknife.unbind(this);
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        icepick.saveInstanceState(this, outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        icepick.restoreInstanceState(this, savedInstanceState);
    }

}
