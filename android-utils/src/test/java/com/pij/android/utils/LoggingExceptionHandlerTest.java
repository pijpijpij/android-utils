package com.pij.android.utils;

import android.util.Log;

import com.pij.android.support.BuildConfig;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author Pierrejean on 30/12/2015.
 */
@Ignore("Something's gone wrong with Robolectric. Will deal with that later")
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.pij.android.support")
public class LoggingExceptionHandlerTest {

    private static final String TAG = "LoggingExceptionHandler";

    private Exception exception;
    private ShadowLog.LogItem expectedLog;

    @Before
    public void initConfig() {
        exception = new Exception();
        expectedLog = new ShadowLog.LogItem(Log.ASSERT, TAG, "Uncaught exception:", exception);
    }

    @Before
    public void resetLogs() {
        ShadowLog.reset();
    }

    @Test
    public void test_nullChain_DoesNothingSpecial() {
        new LoggingExceptionHandler(null);
    }

    @Test
    public void test_uncaughtException_callsChain() {
        UncaughtExceptionHandler mockChain = mock(UncaughtExceptionHandler.class);
        LoggingExceptionHandler tested = new LoggingExceptionHandler(mockChain);

        Thread thread = new Thread();
        tested.uncaughtException(thread, exception);

        Mockito.verify(mockChain).uncaughtException(thread, exception);
    }

    @Test
    public void test_uncaughtException_logs() {
        UncaughtExceptionHandler mockChain = mock(UncaughtExceptionHandler.class);
        LoggingExceptionHandler tested = new LoggingExceptionHandler(mockChain);

        tested.uncaughtException(new Thread(), exception);

        List<ShadowLog.LogItem> logs = ShadowLog.getLogsForTag(TAG);
        assertThat(logs).isNotEmpty();
        assertThat(logs.get(0)).isEqualTo(expectedLog);
    }

    @Test
    public void test_uncaughtExceptionWithNullChain_logs() {
        LoggingExceptionHandler tested = new LoggingExceptionHandler(null);

        tested.uncaughtException(new Thread(), exception);

        List<ShadowLog.LogItem> logs = ShadowLog.getLogsForTag(TAG);
        assertThat(logs).isNotEmpty();
        assertThat(logs.get(0)).isEqualTo(expectedLog);
    }
}