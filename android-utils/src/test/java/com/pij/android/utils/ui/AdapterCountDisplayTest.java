package com.pij.android.utils.ui;

import android.content.res.Resources;
import android.database.DataSetObserver;
import android.widget.Adapter;
import android.widget.TextView;

import com.pij.android.utils.widget.AdapterCountDisplay;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Matchers;
import org.mockito.Mockito;

/**
 * @author Pierrejean on 14/06/2015.
 */
public class AdapterCountDisplayTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private Adapter mockAdapter;
    private Resources mockResources;
    private TextView mockTarget;

    @Before
    public void setUpDefaultEnvironment() {
        mockAdapter = Mockito.mock(Adapter.class);
        mockTarget = Mockito.mock(TextView.class);
        mockResources = Mockito.mock(Resources.class);
        Mockito.when(mockTarget.getResources()).thenReturn(mockResources);
    }

    AdapterCountDisplay createDefaultTested() {
        //noinspection ResourceType
        return new AdapterCountDisplay(mockAdapter, 1);
    }

    @Test
    public void test_NullAdapter_Throws() {
        thrown.expect(NullPointerException.class);

        //noinspection ConstantConditions
        new AdapterCountDisplay(null, 0);
    }

    @Test
    public void test_SetTarget_RegistersObserver() {
        Mockito.when(mockAdapter.getCount()).thenReturn(23);
        //noinspection ResourceType
        Mockito.when(mockResources.getQuantityString(1, 23, 23)).thenReturn("the text");
        AdapterCountDisplay tested = createDefaultTested();

        tested.setTarget(mockTarget);

        Mockito.verify(mockAdapter).registerDataSetObserver(Matchers.any(DataSetObserver.class));
    }

    @Test
    public void test_SetTarget_UpdatesTarget() {
        Mockito.when(mockAdapter.getCount()).thenReturn(23);
        //noinspection ResourceType
        Mockito.when(mockResources.getQuantityString(1, 23, 23)).thenReturn("the text");
        AdapterCountDisplay tested = createDefaultTested();

        tested.setTarget(mockTarget);

        Mockito.verify(mockTarget).setText("the text");
    }

    @Test
    public void test_SetTargetNull_UnregistersObserver() {
        AdapterCountDisplay tested = createDefaultTested();

        tested.setTarget(null);

        Mockito.verify(mockAdapter).unregisterDataSetObserver(Matchers.any(DataSetObserver.class));
    }

    @Test
    public void test_ObserverCalled_UpdatesTarget() {
        Mockito.when(mockAdapter.getCount()).thenReturn(23);
        //noinspection ResourceType
        Mockito.when(mockResources.getQuantityString(1, 23, 23)).thenReturn("the text");
        //noinspection ResourceType
        Mockito.when(mockResources.getQuantityString(1, 23, 23)).thenReturn("another");
        AdapterCountDisplay tested = createDefaultTested();

        tested.setTarget(mockTarget);

        Mockito.verify(mockTarget).setText("another");
    }
}