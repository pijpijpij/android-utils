package com.pij.util.rx;

import android.support.annotation.NonNull;

import java.util.concurrent.atomic.AtomicLong;

import rx.functions.Action0;

/**
 * <p>Created on 02/07/2016.</p>
 * @author Pierrejean
 */
public class CountingHook implements Hook {

    private final AtomicLong callCount = new AtomicLong(0);
    private final Hook decorated;

    public CountingHook(@NonNull Hook decorated) {
        this.decorated = decorated;
    }

    @Override
    public void onStart() {
        System.out.println("RxHook starting to use an instance of: " + getNameOfDecorated());
        decorated.onStart();
    }

    @NonNull
    @Override
    public Action0 onSchedule(@NonNull Action0 action) {
        long currentCount = callCount.incrementAndGet();
        System.out.println("RxHook.onSchedule called " + currentCount + " times (" + getNameOfDecorated() +
                           ", this time on " + action + ")");
        return decorated.onSchedule(action);
    }

    @Override
    public void onStop() {
        System.out.println("RxHook total calls=" + callCount.get() + " for Hook " + getNameOfDecorated());
        decorated.onStop();
    }

    private String getNameOfDecorated() {
        return decorated.getClass().getName();
    }

}
