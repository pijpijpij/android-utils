package com.pij.util.rx;

import android.support.annotation.NonNull;

import rx.Observable;
import rx.Scheduler;
import rx.Single;

/**
 * @author Pierrejean on 14/11/2015.
 */
public final class RxUtil {

    @NonNull
    public static <T> Observable.Transformer<T, T> background(@NonNull final Scheduler background,
                                                              @NonNull final Scheduler forefront) {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> observable) {
                return observable.subscribeOn(background).observeOn(forefront);
            }
        };
    }

    @NonNull
    public static <T> Single.Transformer<T, T> backgroundSingle(@NonNull final Scheduler background,
                                                                @NonNull final Scheduler forefront) {
        return new Single.Transformer<T, T>() {
            @Override
            public Single<T> call(Single<T> single) {
                return single.subscribeOn(background).observeOn(forefront);
            }
        };
    }

    /**
     * To avoid instantiation.
     */
    private RxUtil() { }
}
