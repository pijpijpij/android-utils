package com.pij.util.rx;


import android.support.annotation.NonNull;

import rx.functions.Action0;
import rx.plugins.RxJavaSchedulersHook;

/**
 * <p>Set this up at the start of any RxJava application so the hook can dynamically be changed later during the
 * lifetime</p> of the application. <p>Created on 13/06/2016.</p>
 * @author Pierrejean
 */
public final class RxSchedulersHookedHook extends RxJavaSchedulersHook {

    public static final Hook PASS_THROUGH = new Hook() {
        @Override
        public void onStart() {
            // Does nothing
        }

        @NonNull
        @Override
        public Action0 onSchedule(@NonNull Action0 action) {
            return action;
        }

        @Override
        public void onStop() {
            // Does nothing
        }
    };

    private static RxSchedulersHookedHook instance;
    private Hook delegate;

    public static synchronized RxSchedulersHookedHook getInstance() {
        if (instance == null) instance = new RxSchedulersHookedHook();
        return instance;
    }

    private RxSchedulersHookedHook() {
        delegate = PASS_THROUGH;
        // Apparently redundant, but ensures this class is independent of the implementation of PASS_THROUGH.
        delegate.onStart();
    }

    @Override
    public Action0 onSchedule(Action0 action) {
        return delegate.onSchedule(action);
    }

    public void setDelegate(final Hook newValue) {
        delegate.onStop();
        delegate = newValue == null ? PASS_THROUGH : newValue;
        delegate.onStart();
    }

}
