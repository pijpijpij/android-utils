package com.pij.util.rx;

import android.support.annotation.NonNull;

import rx.functions.Action0;

/**
 * <p>Created on 02/07/2016.</p>
 * @author Pierrejean
 */
public interface Hook {

    void onStart();

    @NonNull
    Action0 onSchedule(@NonNull Action0 action);

    void onStop();

}
