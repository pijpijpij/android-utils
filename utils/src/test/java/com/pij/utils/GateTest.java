package com.pij.utils;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Pierrejean
 */
public class GateTest {

    @Mock
    private Gate.Action<String> mockAction;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void test_unOpenedGateWithNullParameters_doesNotExplode() {
        Gate<String> tested = createDefaultSut();

        tested.perform(null);
    }

    @Test
    public void test_unOpenedGateWithNonNullParameters_doesNotExplode() {
        Gate<String> tested = createDefaultSut();

        tested.perform("Not null!");
    }

    @Test
    public void test_open_returnsTheGate() {
        Gate<String> tested = createDefaultSut();

        assertThat(tested.open(mockAction)).isSameAs(tested);
    }

    @Test
    public void test_performThenOpen_callsAction() {
        Gate<String> tested = createDefaultSut();

        tested.perform("something");
        tested.open(mockAction);

        verify(mockAction).call("something");
    }

    @Test
    public void test_openThenPerform_callsAction() {
        Gate<String> tested = createDefaultSut();

        tested.open(mockAction);
        tested.perform("something");

        verify(mockAction).call("something");
    }

    @Test
    public void test_performThenMultipleOpenAndClose_callsActionOnlyOnce() {
        Gate<String> tested = createDefaultSut();
        tested.perform("tada");

        tested.open(mockAction);
        tested.close();
        tested.open(mockAction);

        verify(mockAction, times(1)).call("tada");
    }

    @Test
    public void test_performWhenClose_delaysActionToWhenOpen() {
        Gate<String> tested = createDefaultSut();
        tested.open(mockAction);
        tested.close();
        verifyZeroInteractions(mockAction);

        tested.perform("tada");
        tested.open(mockAction);

        verify(mockAction).call("tada");
    }

    private Gate<String> createDefaultSut() {
        return new Gate<>();
    }
}