package com.pij.utils;

import org.junit.Test;

import java.util.Date;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;

/**
 * @author Pierrejean on 16/09/2015.
 */
public class DateUtilsTest {

    @Test
    public void test_NullTime_Null() {
        assertThat(com.pij.utils.DateUtils.clone(null), nullValue(Date.class));
    }

    @Test
    public void test_ClonedTime_NotSame() {
        final Date time = new Date();
        assertThat(com.pij.utils.DateUtils.clone(time), not(sameInstance(time)));
    }

    @Test
    public void test_ClonedTime_Equals() {
        final Date time = new Date();
        assertThat(com.pij.utils.DateUtils.clone(time), equalTo(time));
    }

}