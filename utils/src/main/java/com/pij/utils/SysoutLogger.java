/*
 * Copyright 2018, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package com.pij.utils;

import android.support.annotation.NonNull;

public class SysoutLogger implements Logger {

    private void print(Class<?> clazz, String message) {
        System.out.println(clazz.getSimpleName() + " " + message);
    }

    @Override
    public void print(@NonNull Class<?> clazz, @NonNull String messageTemplate, @NonNull Object... args) {
        print(clazz, String.format(messageTemplate, args));
    }

    @Override
    public void print(@NonNull Class<?> clazz, @NonNull Throwable error, @NonNull String messageTemplate, @NonNull Object... args) {
        String message = String.format(messageTemplate, args);
        System.out.println(clazz.getSimpleName() + " " + message);
        error.printStackTrace();
    }

}