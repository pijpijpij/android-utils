package com.pij.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author pierrejean
 */
public abstract class ObjectUtils extends org.apache.commons.lang3.ObjectUtils {

    /**
     * The only point of this is to eliminate the deprecation warning on {@link org.apache.commons.lang3.ObjectUtils#toString(Object,
     * String)}.
     * @see {@link org.apache.commons.lang3.ObjectUtils#toString(Object, String)}
     */
    @SuppressWarnings("deprecation")
    @NonNull
    public static String toString(@NonNull Object toStringify, @Nullable final String defaultString) {
        return org.apache.commons.lang3.ObjectUtils.toString(toStringify, defaultString);
    }

    /**
     * To allow extension.
     */
    protected ObjectUtils() {

        // Does nothing.
    }

}
