package com.pij.utils;

import java.util.Date;

/**
 * @author pierrejean
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {

    /**
     * Convenience method to avoid littering code with <code>if (time == nul)...</code>
     * @param time may be <code>null</code>
     * @return a copy of <code>time</code>, null if <code>time</code> was <code>null</code>.
     */
    public static Date clone(final Date time) {
        return time == null ? null : (Date)time.clone();
    }

    /**
     * To allow extension.
     */
    protected DateUtils() {
        // Does nothing.
    }

}
