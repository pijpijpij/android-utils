package com.pij.utils;

import android.support.annotation.NonNull;

/**
 * <p>Created on 22/03/2018.</p>
 *
 * @author Pierrejean
 */

public interface Logger {

    @NonNull
    Logger NOOP = new Logger() {
        @Override
        public void print(@NonNull Class<?> clazz, @NonNull String messageTemplate, @NonNull Object... args) {

        }

        @Override
        public void print(@NonNull Class<?> clazz, @NonNull Throwable error, @NonNull String message, @NonNull Object... args) {

        }
    };

    void print(@NonNull Class<?> clazz, @NonNull String messageTemplate, @NonNull Object... args);

    void print(@NonNull Class<?> clazz, @NonNull Throwable error, @NonNull String messageTemplate, @NonNull Object... args);
}
