package com.pij.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author pierrejean
 */
public abstract class SystemUtils {

    /**
     * Utility method that provides a simple tag for an object. The tag is the simple class name of the argument
     * <code>tagged</code>.
     * @param tagged may be <code>null</code>.
     * @return simple class name of the argument <code>tagged</code>.
     * @deprecated use {@link SystemUtils#tag(Object)} } instead
     */
    @NonNull
    public static String getTag(@Nullable final Object tagged) {
        return tag(tagged);
    }

    /**
     * Utility method that provides a simple tag for an object. The tag is the simple class name of the argument
     * <code>tagged</code>.
     * @param tagged may be <code>null</code>.
     * @return simple class name of the argument <code>tagged</code>.
     */
    @NonNull
    public static String tag(@Nullable final Object tagged) {

        if (tagged == null) {
            return "null Tag!";
        }

        return tag(tagged.getClass());
    }

    /**
     * Utility method that provides a simple tag for a class. The tag is the simple name of the argument.
     * @param tagged may be <code>null</code>.
     * @deprecated use {@link SystemUtils#tag(Object)} } instead
     */
    @NonNull
    public static String getTag(@NonNull Class<?> tagged) {
        return tag(tagged);
    }

    /**
     * Utility method that provides a simple tag for a class. The tag is the simple name of the argument.
     * @param tagged may be <code>null</code>.
     * @return simple name of the class <code>tagged</code>
     */
    @NonNull
    public static String tag(Class<?> tagged) {

        if (tagged == null) {
            return "null Tag!";
        }
        String name = tagged.getName();
        final String packag = tagged.getPackage().getName();
        final int packagestart = name.indexOf(packag);
        if (packagestart >= 0) {
            name = name.substring(packagestart + packag.length() + 1, name.length());
        }

        return name;
    }

    /**
     * To allow extension.
     */
    protected SystemUtils() {

        // Does nothing.
    }

}
