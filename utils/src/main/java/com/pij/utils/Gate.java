package com.pij.utils;

import android.support.annotation.NonNull;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * If closed, it stores the last value "{@link #perform(Object)}ed". When it opens, is run its action on the value
 * currently stored; it then runs it for all values ""{@link #perform(Object)}ed", until it is closed again.
 * @author Pierrejean
 */
public class Gate<T> {

    private Action<T> action;
    @SuppressWarnings("unchecked")
    private Stash<T> latest = new Stash();

    public synchronized Gate<T> open(@NonNull Action<T> action) {
        this.action = notNull(action);
        if (latest.isNotEmpty()) {
            perform(latest.extract());
        }
        return this;
    }

    public synchronized void close() {
        action = null;
    }

    public synchronized void perform(T parameters) {
        if (action == null) {
            latest.put(parameters);
        } else {
            action.call(parameters);
        }
    }

    /**
     * A one-argument action, clearly "inspired" by RxJava.
     */
    public interface Action<T> {

        void call(T t);
    }

    private static class Stash<T> {

        private boolean hasData;
        private T value;

        public void put(T newValue) {
            value = newValue;
            hasData = true;
        }

        public T extract() {
            hasData = false;
            //noinspection unchecked
            T tmp = value;
            value = null;
            return tmp;
        }

        public boolean isNotEmpty() {
            return hasData;
        }
    }
}
