# README

## What is is?

This is a set of java and Android libraries gathering all sorts of Android-specific utilities I found I needed at some
point or other.
So there is probably not a lot of coherence. Here are some of the features.
- a Matcher for Hamcrest 1.3 in [Hamcrest](/hamcrest)
- [Android](/android-utils)
- RxJava:
  - [production utilities](rx-utils)
  - [test utilities](rx-test)
- RxJava and Android:
  - [production utilities](rx-android-utils)
  - [test utilities](rx-android-test)
- plain [Java](/utils)

# Building and Releasing the libraries

## Automated build status
Bitbucket pipelines is the CI.

The binaries of releases are also available thanks to [Jitpack](https://jitpack.io). The latest release there is 
[![](https://jitpack.io/v/org.bitbucket.pijpijpij/android-utils.svg)](https://jitpack.io/#org.bitbucket.pijpijpij/android-utils).

## Build and install the libraries locally?

`> gradlew build` builds the project
`> gradlew install` places it in the local Maven repository.

## How to release the libraries?

That means creating a release version and prepare it for the next release increment. That includes setting up its SCM.
Releases are tagged with their version number (e.g. release 5.3.2 is build from he version tagged `5.3.2` in Git).

1. Checkout the head of `master` and start a command prompt
1. Run pre-release checks. Do a full build to ensure the code is good to be released.

    `> ./gradlew build`

1. Release (assuming authentication with SSH keys is already setup. If not, Bitbucket explained it well 
[here](https://confluence.atlassian.com/x/YwV9E)):

    `> ./gradlew release`

    Make sure the last output line indicates it's been *pushed to origin*.

    To set the release number, rather than accept the usual bug-level increment, add the following property on the 
    command line:

    `-Prelease.forceVersion=k.m.n`

1. Build the release version of the app to take the new version number into account:

    `> ./gradlew build install`
    
    That is only needed if you do not want to wait for [Jitpack](https://jitpack.io/#org.bitbucket.pijpijpij/android-utils/) to finish its 
    build.


The overall command is quite simple:

    > ./gradlew build release

# Miscellaneous

## Next features
Generate a decorator for non-final classes with non-final public methods.


## Download
Only source download available, from this site! Releases are tagged.

## Usage
To use, download the source for the version of interest, build it and install it in your local Maven repository.
Then, in a Maven project, where the Hamcrest library is required:
```xml
<dependency>
  <groupId>com.bitbucket.pijpijpij.android-utils</groupId>
  <artifactId>hamcrest</artifactId>
  <scope>test</scope>
  <version>1.2.4</version>
</dependency>
```
or Gradle for an Android project:
```groovy
dependencies {
  compile 'com.bitbucket.pijpijpij.android-utils:hamcrest:1.2.4'
}
```
With an older Android Gradle plugin, use [Hugo Visser](https://bitbucket.org/hvisser/android-apt)'s `apt` instead of 
`annotationProcessor`. 


## My License
    Copyright 2015 PJ Champault

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


## Other Licenses
There are so many OS libraries of great value. I've used a lot here. The top pick: Apache still rocks!
Thank you to all the others too...


